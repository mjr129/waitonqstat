=============================
WAITONQSTAT - Wait on "qstat"
=============================

Calls ``qstat``:t: in a loop until no more jobs remain.

-----
Usage
-----

::

    waitonqstat
