import re
import subprocess
from os import listdir, path
from time import sleep


def read_all_text( file_name : str ) -> str:
    with open( file_name, "r" ) as file:
        return file.read()

def main() -> None:
    text : str = "1"
    next_wait : int = 1
    
    while text:
        print("Polling qstat")
        result = subprocess.run(["qstat"], stdout=subprocess.PIPE)
        text : str = result.stdout.decode('utf-8').strip()
        
        if text:
            sleep(next_wait)
            next_wait += 1
            
    for file in listdir("."):
        ext : str = path.splitext(file)[1]
        if re.match("^__.*\.[oe][0-9]+$", file) is not None:
            text = read_all_text(file)
            print("==================== {} ====================".format(file))
            print(text)
            print()

if __name__ == "__main__":
    main()
