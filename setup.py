from distutils.core import setup


setup( name = "waitonqstat",
       version = "1.0.0.0",
       description = "Calls qstat until no more jobs remain.",
       author = "Martin Rusilowicz",
       license = "https://www.gnu.org/licenses/agpl-3.0.html",
       url = "https://bitbucket.org/mjr129/waitonqstat",
       python_requires = ">=3.6",
       packages = ["waitonqstat"],
       install_requires = [],
       entry_points = { "console_scripts": ["waitonqstat = waitonqstat.__main__:main"] },
       )
